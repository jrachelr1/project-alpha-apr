from django.contrib import admin
from tasks.models import Task


# Register your models here.
class TaskAdmin(Task):
    pass


admin.site.register(Task)
